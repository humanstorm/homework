# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Band.create(name: 'Timbaland', num_members: 4)
Band.create(name: 'Fiber', num_members: 2)
Band.create(name: 'Buffalo Springfield', num_members: 5)
Band.create(name: 'Wild West', num_members: 4)

Club.create(name: 'Limelight Club', street_address: '28572 Washington')
Club.create(name: 'Krypto Club', street_address: '128 Will St')
