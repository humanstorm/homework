class Booking < ActiveRecord::Base
  belongs_to :band
  belongs_to :club
  validates :fee, :numericality => { :greater_than_or_equal_to => 0 }
end
