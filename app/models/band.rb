class Band < ActiveRecord::Base
  has_many :bookings
  has_many :clubs, :through => :bookings
end
